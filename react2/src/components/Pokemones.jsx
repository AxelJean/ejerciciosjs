import React from 'react' // En pokeDucks.js : estan Constantes iniales(), Reducer , Acciones
// 2 - Este componente principal va disparar la accion(eventos).. envia a pokeDucks.js
// para eso se va importar pokeDucks.js, abajo
//Con el buttom(Accion) ejecuta el dispatch(obtenerPokeAction())..obtenerPokeAction es
//es la Accion que esta en pokeDucks.js
// y con la const pokemones, recogemos nuevo valor del Store.js para pintarlo web




// hooks react redux
import {useDispatch, useSelector} from 'react-redux'

// importamos la acción
import {obtenerPokeAction} from '../redux/pokeDucks'
import {siguientePokeAction} from '../redux/pokeDucks'

const Pokemones = () => {

    // declaramos displach para llamar a la acción o acciones
    const dispatch = useDispatch()

    // crearmos el state utilizando nuestra tienda
    // store.pokemones lo sacamos de la tienda
    //useSelector, permite extraer datos de una store de Redux
    const pokemones = useSelector(store => store.pokemones.array)

    return (
        <div>
            <h1>Pokemones!</h1>
            <button onClick={() => dispatch(obtenerPokeAction())}>Obtener</button>
            <button onClick={() => dispatch(siguientePokeAction(20))}>Siguientes</button>
            <ul>
                {
                    pokemones.map(item => (
                        <li key={item.name}>{item.name}</li>
                    ))
                }
            </ul>
        </div>
    )
}

export default Pokemones