import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import pokesReducer from './pokeDucks' // por defecto toma el Reducer

const rootReducer = combineReducers({
    pokemones: pokesReducer //4 - este pokesReducer(pokeDucks.js) es el que cambia y retorna nuevo valor State
})                          // 5- el pokesReducer me retorna stado nuevo(return) va ser acumulado este Store
                            // va ser utilizado en Pokemones.jsx
                            // pokemones: es obtiene nuevo valor state, va ser utilizado en Pokemones.jsx

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
    const store = createStore( rootReducer, composeEnhancers( applyMiddleware(thunk) ) )
    return store
}