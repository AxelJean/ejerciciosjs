
import React from 'react';
import {Provider} from 'react-redux'
import generateStore from './redux/store'// 1- acceso al store.js, principal
import Pokemones from './components/Pokemones';// Provider va envolverlo, todo componentes, sea usado estados que cambian

function App() {
  const store = generateStore()
  return (
    <Provider store={store}>
      <Pokemones />
    </Provider>
  );
}

export default App;
