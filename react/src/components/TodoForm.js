import React, { Component } from 'react';

class TodoForm extends Component {
  constructor () {
    super();
    this.state = {
      title: '',
      responsible: '',
      description: '',
      priority: 'low'
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {  // hace 2 cosas : sobreescribe estado local y crea prop(de funcion) para ser usado 
    e.preventDefault();
    this.props.onAddTodo(this.state);//2- (this.state) igual objeto..onAddTodo es funcion con parametro(objeto llenado), va ser usado en el principal
    this.setState({            // 1- Al presionar Boton(local)este sobreescribe estado
      title: '',               //onAddTodo(this.state) . onAddTodo es estatico(prop) aqui, Parametro esta lleno.. es llevado al principal sea ejecutado, al ser igualado
      responsible: '',
      description: '',
      priority: 'low'
    });
  }

  handleInputChange(e) {
    const {value, name} = e.target;// objeto...elemento lanzo el evento 
    console.log(value, name);  // ( abx(el texto ingresado) , title(name) )
    this.setState({
      [name]: value  // sobrescribe estado .. title = abx
    });
    console.log('estado actual del form',this.state);
  }

  render() {
    return (
      <div className="card">
        <div>{this.props.titulo}</div>
        <form onSubmit={this.handleSubmit} className="card-body">
          <div className="form-group">
            <input
              type="text"
              name="title"
              className="form-control"
              value={this.state.title}
              onChange={this.handleInputChange}
              placeholder="Title"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="responsible"
              className="form-control"
              value={this.state.responsible}
              onChange={this.handleInputChange}
              placeholder="Responsible"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="description"
              className="form-control"
              value={this.state.description}
              onChange={this.handleInputChange}
              placeholder="Description"
              />
          </div>
          <div className="form-group">
            <select
                name="priority"
                className="form-control"
                value={this.state.priority}
                onChange={this.handleInputChange}
              >
              <option>low</option>
              <option>medium</option>
              <option>high</option>
            </select>
          </div>
          <button type="submit" className="btn btn-primary">
            Save
          </button>
        </form>
      </div>
    )
  }

}

export default TodoForm;
