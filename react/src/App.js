import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// data
import { todosx } from './todos.json';// = "todosx": [{"title": "task1","responsible": "mark","description": "lorem impsum","priority": "low"},

// subcomponents
import TodoForm from './components/TodoForm';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos:todosx // todos = {"todos": [{"title": "task1","responsible": "mark","description": "lorem impsum","priority": "low"},
    }       //                    {"title": "task1","responsible": "mark","description": "lorem impsum","priority": "low"},
            //                   ]
            //         }
    this.handleAddTodo = this.handleAddTodo.bind(this);
    console.log( todosx );
    console.log(this.state.todos);
  }

  removeTodo(index) {
    this.setState({
      todos: this.state.todos.filter((e, i) => {
        return i !== index
      })
    });
  }

  handleAddTodo(todo) { // este parametro viene otro componente ya llenado(es objeto, era su estado)
    console.log('1',this.state.todos)
    console.log('2',todo)
    this.setState({
      todos: [...this.state.todos, todo] // va agregarlo, ...this.state.todos es array de objeto que se le va agregar un nuevo elemento (objeto)
    })    
  }

  render() {
    const todos = this.state.todos.map((todo, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card mt-4">
            <div className="card-title text-center">
              <h3>{todo.title}</h3>
              <span className="badge badge-pill badge-danger ml-2">
                {todo.priority}
              </span>
            </div>
            <div className="card-body">
              {todo.description}
            </div>
            <div className="card-footer">
              <button
                className="btn btn-danger"
                onClick={this.removeTodo.bind(this, i)}>
                Delete
              </button>
            </div>
          </div>
        </div>
      )
    });

    // RETURN THE COMPONENT   // aqui {this.handleAddTodo} ejecuta la funcion esta otro componente.. como innerjoin lo pasa 
    return (
      <div className="App">

        <nav className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="/">
            Tasks
            <span className="badge badge-pill badge-light ml-2">
              {this.state.todos.length}
            </span>
          </a>
        </nav>

        <div className="container">
          <div className="row mt-4">
              <div className="col-md-4 text-center">
                  <img src={logo} className="App-logo" alt="logo" />
                  <TodoForm onAddTodo={this.handleAddTodo} titulo="Llenar el Formulario"></TodoForm>
              </div>

              <div className="col-md-8">
                  <div className="row">
                    {todos}
                  </div>
              </div>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
