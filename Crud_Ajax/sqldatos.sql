-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-08-2020 a las 19:54:53
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `paginaweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `idart` int(11) NOT NULL,
  `tituloart` varchar(250) CHARACTER SET utf8 NOT NULL,
  `textoart` text CHARACTER SET utf8 NOT NULL,
  `imagenart` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`idart`, `tituloart`, `textoart`, `imagenart`) VALUES
(1, 'Titulo 1', 'La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.', '12.png'),
(2, 'Titulo 2', 'La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.', '13.png'),
(3, 'Titulo 3', 'La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.', '14.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `axel`
--

CREATE TABLE `axel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `imgbanner` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`id`, `imgbanner`, `nombre`) VALUES
(1, 'img_nature_wide.jpg', 'imagen1'),
(2, 'img_snow_wide.jpg', 'imagen2'),
(3, 'img_lights_wide.jpg', 'imagen3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bcps`
--

CREATE TABLE `bcps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `animal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `bcps`
--

INSERT INTO `bcps` (`id`, `animal`, `nombre`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Tigre', 'Axel', 'axelromani@hotmail.com', NULL, NULL),
(2, 'Leon', 'Fabiane', 'jmphilario@yahoo.es ', NULL, NULL),
(3, 'Puma', 'Santiago', 'spastor@minam.gob.pe', NULL, NULL),
(4, 'Guepardo', 'Juan', 'irogovich@spda.org.pe', NULL, NULL),
(5, 'jirafa', 'Manuel', 'msiguenas@inia.gob.pe', NULL, NULL),
(6, 'Elefante', 'Christophe', 'Christoph.Spennemann@unctad.org', NULL, NULL),
(7, 'Gallo', 'David', 'dvivas@ictsd.ch', NULL, NULL),
(8, 'Pez', 'Brooks', 'bshaffer@ictsd.ch', NULL, NULL),
(9, 'Gorila', 'Paul', 'poldham@mac.com', NULL, NULL),
(10, 'Perro', 'Tomas', 'aneyra@rree.gob.pe', NULL, NULL),
(11, 'Gato', 'Mauricio', 'onuginebra@mmrree.gov.ec', NULL, NULL),
(12, 'Cerdo', 'Rainer', 'rainer.engels@gtz.de', NULL, NULL),
(13, 'Loro', 'Lorenzo', 'Lorenzo@homail.com', '2020-03-14 22:36:36', '2020-03-14 22:36:36'),
(16, 'Loro', 'Lorenzo', 'Lorenzo@hoHGmail.com', '2020-03-14 22:42:37', '2020-03-14 22:42:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idcategoria` int(11) NOT NULL,
  `nombrecategoria` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idcategoria`, `nombrecategoria`) VALUES
(1, 'catego 1'),
(2, 'catego 2'),
(3, 'catego 3'),
(4, 'catego 4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre`, `direccion`, `email`, `password`) VALUES
(1, 'mario', 'chorrillos', 'mariogmail.com', 'mario123'),
(2, 'jorge', 'surco', 'jorge@gmail.com', 'jorge123'),
(19, 'manu', 'manu', 'manu', 'manu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datopersonales24`
--

CREATE TABLE `datopersonales24` (
  `codigo24` int(11) NOT NULL,
  `nombre24` varchar(250) CHARACTER SET utf8 NOT NULL,
  `paterno24` varchar(250) CHARACTER SET utf8 NOT NULL,
  `materno24` varchar(250) CHARACTER SET utf8 NOT NULL,
  `fechanac24` date NOT NULL,
  `edad24` int(11) NOT NULL,
  `equipo24` varchar(250) NOT NULL,
  `telefono24` int(11) NOT NULL,
  `email24` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datopersonales24`
--

INSERT INTO `datopersonales24` (`codigo24`, `nombre24`, `paterno24`, `materno24`, `fechanac24`, `edad24`, `equipo24`, `telefono24`, `email24`) VALUES
(1, 'AXEL', 'Romani', 'En', '2019-03-19', 25, 'AL', 4528996, 'axelro94@hotmail.com'),
(2, 'RAW', 'TEDE', 'GRE', '2019-03-20', 25, 'AL', 4568342, 'TTTT@hotmail.com'),
(3, 'martin', 'RT', 'weee', '2019-03-23', 30, 'ER', 44444, '931126177@HOTMAIL.COM'),
(4, 'Franco', 'James', 'Martinez', '2019-08-10', 25, 'Lakers', 85769479, 'Lakers@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `datos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `Paterno` varchar(250) NOT NULL,
  `Materno` varchar(250) NOT NULL,
  `FechaNac` date NOT NULL,
  `Edad` int(11) NOT NULL,
  `Equipo` varchar(250) NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mensaje` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datos`
--

INSERT INTO `datos` (`id`, `nombre`, `Paterno`, `Materno`, `FechaNac`, `Edad`, `Equipo`, `telefono`, `email`, `mensaje`) VALUES
(1, 'daniel', '', '', '0000-00-00', 0, '', 2342343, 'prueba@gmail.com', 'este es el mensaje ...'),
(2, 'jorge', '', '', '0000-00-00', 0, '', 97655656, 'prueba@gmail.com', 'todo el mensaje ...'),
(3, 'daniel', '', '', '0000-00-00', 0, '', 2342343, 'prueba@gmail.com', 'z'),
(4, 'AXEL', '', '', '0000-00-00', 0, '', 65464, 'axelrom94@hotmail.com', 'uuuuu'),
(31, 'Harry', 'Merkel', 'Polittico', '2019-08-10', 25, 'Amarillo', 99587412, 'Amarillo@gmail.com', 'Hey soy Amarillo'),
(32, 'TITO', 'TUE', 'DAME', '2019-08-15', 80, 'AZUL', 6546454, 'axelrom94@hotmail.com', 'guau!!'),
(33, 'TITO', 'TUE', 'DAME', '2019-08-15', 80, 'AZUL', 6546454, 'axelrom94@hotmail.com', 'guau!!');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id_detpedido` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` double NOT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id_detpedido`, `id_pedido`, `id_producto`, `cantidad`, `precio`) VALUES
(4, 15, 1, 123, 20),
(5, 17, 1, 231, 13),
(6, 17, 2, 75, 5),
(7, 18, 1, 236, 13),
(8, 18, 2, 75, 5),
(9, 20, 1, 2, 13),
(10, 21, 1, 2, 13),
(11, 21, 2, 3, 5),
(12, 21, 3, 5, 8),
(13, 22, 1, 2, 13),
(14, 22, 2, 3, 5),
(15, 22, 3, 5, 8),
(16, 23, 1, 2, 13),
(17, 24, 3, 5, 8),
(18, 24, 2, 7, 5),
(19, 25, 1, 12, 13),
(20, 25, 3, 2, 8),
(21, 26, 2, 4, 5),
(37, 38, 1, 2, 13),
(38, 38, 3, 8, 8),
(58, 45, 1, 3, 13),
(59, 45, 2, 5, 5),
(60, 45, 3, 4, 8),
(61, 46, 3, 4, 8),
(62, 46, 1, 3, 13),
(63, 47, 2, 37, 5),
(64, 47, 1, 3, 13),
(65, 47, 3, 3, 8),
(66, 48, 2, 17, 5),
(67, 48, 3, 2, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `titulo`, `url`) VALUES
(1, 'Politica', 'politica.php'),
(2, 'Mundo', 'mundo.php'),
(3, 'Economía', 'economia.php'),
(4, 'Deportes', 'deportes.php'),
(5, 'Entretet', 'Entretet.php'),
(6, 'Salud', 'salud.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2020_02_29_212232_create_ventas_table', 1),
(5, '2020_02_29_223708_create_axel_table', 2),
(6, '2020_03_08_172747_create_personas_table', 3),
(7, '2020_03_08_173752_create_empleados_table', 4),
(8, '2020_03_12_214547_crear_tabla_rimac', 5),
(12, '2020_03_12_224123_crear_tabla_rimacs', 6),
(14, '2014_10_12_000000_create_users_table', 7),
(15, '2019_08_19_000000_create_failed_jobs_table', 7),
(16, '2020_03_13_032347_crear_tabla_bcp', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `texto` text NOT NULL,
  `fecha` date NOT NULL,
  `imagen` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `texto`, `fecha`, `imagen`) VALUES
(22, '', '', '0000-00-00', ''),
(23, '', '', '0000-00-00', ''),
(24, '', '', '0000-00-00', ''),
(25, '', '', '0000-00-00', ''),
(26, '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `monto` double NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `id_cliente`, `monto`, `estado`, `fecha`) VALUES
(4, 1, 123, 0, '2020-07-08'),
(5, 1, 234, 0, '2020-07-08'),
(6, 1, 3124, 0, '2020-07-08'),
(7, 1, 3124, 0, '2020-07-08'),
(8, 1, 3173, 0, '2020-07-08'),
(9, 1, 3183, 0, '2020-07-08'),
(10, 1, 3183, 0, '2020-07-08'),
(11, 1, 3183, 0, '2020-07-08'),
(12, 1, 3183, 0, '2020-07-08'),
(13, 1, 3183, 0, '2020-07-08'),
(14, 1, 3183, 0, '2020-07-08'),
(15, 1, 3248, 0, '2020-07-08'),
(16, 1, 3313, 0, '2020-07-08'),
(17, 1, 3378, 0, '2020-07-08'),
(18, 1, 3443, 0, '2020-07-08'),
(19, 1, 0, 0, '2020-07-08'),
(20, 1, 26, 0, '2020-07-08'),
(21, 1, 81, 0, '2020-07-08'),
(37, 19, 142, 0, '2020-07-10'),
(38, 2, 115, 0, '2020-07-10'),
(39, 2, 120, 0, '2020-07-10'),
(40, 2, 143, 0, '2020-07-10'),
(41, 2, 172, 0, '2020-07-10'),
(42, 2, 122, 0, '2020-07-10'),
(43, 2, 109, 0, '2020-07-10'),
(44, 2, 94, 0, '2020-07-10'),
(45, 2, 96, 0, '2020-07-10'),
(46, 2, 71, 0, '2020-07-10'),
(47, 2, 248, 0, '2020-07-10'),
(48, 1, 101, 0, '2020-08-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `idcategoria1` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `precio` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `idcategoria1`, `nombre`, `precio`, `imagen`, `cantidad`) VALUES
(1, 1, 'libro 1', 13, 'libro1.jpg', 12),
(2, 2, 'libro 2', 5, 'libro2.jpg', 77),
(3, 3, 'libro 3', 8, 'libro3.jpg', 30),
(4, 4, 'libro 4', 23, 'libro4.jpg', 23),
(5, 3, 'libro 5', 45, 'libro5.jpg', 78);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rimacs`
--

CREATE TABLE `rimacs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rimacs`
--

INSERT INTO `rimacs` (`id`, `nombre`, `apellido`, `email`, `created_at`, `updated_at`) VALUES
(1, 'AXEL', 'Romani', 'axelromani@hotmail.com', NULL, NULL),
(2, 'Jean', 'Tremo', 'jeantremo@hotmail.com', NULL, NULL),
(3, 'Julio', 'Alvarado', 'julgas@gmx.net', NULL, NULL),
(4, 'Alejandro', 'Argumedo', 'andes@andes.org.pe', NULL, NULL),
(5, 'Jorge', 'Cabrera', 'jorgecmedaglia@hotmail.com', NULL, NULL),
(6, 'Deyanira', 'Camacho', 'rdcamacho@iepi.gov.ec', NULL, NULL),
(7, 'Xavier', 'Delgado', 'dino@asdmas.com', NULL, NULL),
(8, 'Rainer', 'Engels', 'rainer.engels@gtz.de', NULL, NULL),
(9, 'Antonietta', 'Gutierrez', 'anotnietta@terra.com.pe', NULL, NULL),
(10, 'Hartmut', 'Meyer', 'hmeyer@ensser.org; ', NULL, NULL),
(11, 'Aurora', 'Ortega', 'aortega@indecopi.gob.pe', NULL, NULL),
(12, 'Frank', 'Schmiedchen', 'frank.schmiedchen@bmz.bund.de', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Juan', 'Juan@gmail.com', NULL, NULL),
(2, 'Axel', 'axel@hotmail.com', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`idart`);

--
-- Indices de la tabla `axel`
--
ALTER TABLE `axel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bcps`
--
ALTER TABLE `bcps`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bcps_email_unique` (`email`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `datopersonales24`
--
ALTER TABLE `datopersonales24`
  ADD PRIMARY KEY (`codigo24`);

--
-- Indices de la tabla `datos`
--
ALTER TABLE `datos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id_detpedido`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `empleados_email_unique` (`email`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `rest_usuario` (`id_cliente`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rimacs`
--
ALTER TABLE `rimacs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rimacs_email_unique` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ventas_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `idart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `axel`
--
ALTER TABLE `axel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `bcps`
--
ALTER TABLE `bcps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `datopersonales24`
--
ALTER TABLE `datopersonales24`
  MODIFY `codigo24` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `datos`
--
ALTER TABLE `datos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id_detpedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rimacs`
--
ALTER TABLE `rimacs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD CONSTRAINT `id_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `rest_usuario` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
