<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pagina Php</title>
<link href="css/miEstilox.css" rel="stylesheet"/>
<script src="js/jquery-3.1.1.min.js"></script>

<style>

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin:0;
  padding:0;
  border:0;
}



/*<ul> </ul>  o  <ul class="nombre"></ul>*/

/* En general */
.contenedor{width: 100%;margin-right: auto; margin-left: auto;}
.clear { clear:both}
/* fin general */


/* header */
header .superior{background-color: #ffffff;width:100%;padding-top:10px;padding-bottom:10px;}
header .superior div.logo{ float:left;padding-left:10px; }
header .superior div.bienvenido{ float:left;padding-left:10px; }
.logo img{width: 207px;height:58px}
header .superior div.fecha{float:right;padding-right:10px;}
header .superior div.fecha div{padding-top:20px;padding-bottom:20px;}

.navegacion{width:100%;background-color: #0b6ca7;display:none;}
.navegacion ul{list-style: none;}

.nav{text-align:center; float:left; width:16.5%}
.nav a{width:100%; display: block;  line-height: 50px;color:#ffffff; border-right: 1px solid #00487f;border-left: 1px solid #00487f}
/* fin header */

/* imagen slider */
.imagenslider{width:100%;}
.imagenslider img{width: 100%; height:auto}
/* fin imagen slider */

/* principal */
.principal{background-color: #dbe3e8; padding-left:10px;padding-right:10px;#0b6ca7;}
    /* articulos */
    .articulos{margin-bottom: 10px;}
    .artic{float:left;width:33.333333%;text-align: center;margin-bottom: 5px;}
    .artic img{width: 140px;height:140px}
    .artic h2{margin-bottom:5px}
    .artic p.texto{margin-bottom:10px;margin-left:10px;margin-right:10px}
    
    .artic div.boton{ margin-top:10px;margin-bottom:10px;}
    .artic div.boton a{ margin-top:10px;margin-bottom:10px;color: #fff; background-color: #6c757d; border-color: #6c757d; padding: 6px 10px; font-size: 14px;}    
    /* fin articulos */

    /* noticias */
    .noticias{margin-bottom: 10px;}
    .noticias div.titulo{padding-top:5px;padding-bottom:5px;}
    .noticias div.not{width:100%;background-color: #b5d7ec;margin-bottom:10px;}

    .noticias div.not div.laimagen{float:left;background-color: red;width:20%; }
    .noticias div.not div.laimagen img{width: 100%;height:auto; }
    .noticias div.not .elcontenido{float:left;background-color: #b5d7ec;width:80%;}
    .noticias div.not .elcontenido div{padding-left:10px;}
    .noticias div.not .elcontenido div h2{margin-bottom: 10px;margin-top: 10px;}
    .noticias div.not .elcontenido div p.fecha{margin-bottom: 10px;}
    .noticias div.not .elcontenido div p.parrafo{margin-bottom: 18px;}    
    /* fin noticias */

    /* testimonios */
    .testimonios{width:100%;background-color: #b5d7ec;margin-bottom:10px;}
    .testimonios div.titulo{padding-top:5px;padding-bottom:5px;}
    .testimonios div.testim-contenido{float:left;width:25%;}
    .testimonios div.testim-contenido div.testim-margen{margin-right: 20px;}
    .testimonios div.testim-contenido div.testim-margen img{width: 100%;height:auto;}
    .testimonios div.testim-contenido div.testim-margen .testim-titulo{padding-top:5px;padding-bottom:5px;}
    /* fin testimonios */

    /* footer */
    .footer{padding: 10px;background-color: #053552;margin-right: -10px;margin-left: -10px;}
    .footer .imagen{ float:left;}
    .footer .imagen img{width:40%;}
    .footer .direccion{ float:right; margin-top:30px;color:#ffffff;}

    
    /* fin footer */



/* fin principal */










@media screen and (min-width:580px){
 .contenedor{
  width: 550px;
  
 }



}

@media screen and (min-width:680px){
  .contenedor{
  width: 650px;
  
 }
 .navegacion{

display:block;
}




}

@media screen and (min-width:920px){
  .contenedor{
  width: 890px;
  
 }



}

@media screen and (min-width:1200px){
  .contenedor{
  width: 1100px;
  
 }
}


</style>


<style>
* {box-sizing: border-box}

.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}





/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

<script>
</script>
</head>
<body>

     <?php
        $id=$_GET['id'];
        include'clseventos.php';
        $objContenido = new contenido;
        $sqldetalle = $objContenido -> detallenot($id);
        $campos = mysql_fetch_assoc($sqldetalle);
		
		
		$lasnoticias = $objContenido -> noticias();

       
    ?>
<header>
  <div class="superior">
      <div class="logo" >
         <img  src="imgs/logo.png"  />
      </div>
      <div class="Bienvenido" >
         <?php echo "Bienvenido a la pagina<br><br>";?>
         <?php
          
           if (isset($_POST["nombre"]))
           {
            $nombre = $_POST["nombre"];
            $telefono = $_POST["telefono"];
            $email = $_POST["email"];
            $mensaje = $_POST["mensaje"];            
            $objContenido -> contacto($nombre,$telefono,$email,$mensaje);
            echo "Fue registrado los datos de ". $nombre;
           }
        ?>
      </div>
      <div class="fecha">
         <div> 
          <?php
           $fecha = date("Y/m/d");
           echo "La fecha de hoy es ". $fecha ."<br>";
           //funciones :           
           function lugar() {
            $ciudad="Lima";
            $pais="Peru";            
            echo "<p>Lugar: ".$ciudad." pais de ".$pais."</p>";
           } 
           lugar();
           //
           
           function operacion($n1,$n2) {
            $total = $n1 * $n2;            
            echo "<p>Resultado es: ".$total."</p>";
           } 
           $num1=3; $num2=5;
           operacion($num1,$num2);
           // array y for:
           $equipos = array("Universitario","Alianza","Cristal");
            //var_dump($cars);
           $cantidad= count($equipos);

           for($i=0; $i<$cantidad; $i++){
            echo $equipos[$i]."-";
            }  
            // funciones de php
            $dato = "Instituto peru";  
            echo "<br>cantidad letras: " . strlen($dato);   

          ?>
        </div>
      </div>
      <div class="clear"></div>
  </div>

  <div class="navegacion" >
      <ul  class="contenedor">
        <li class="nav" ><a href="#" >POLITICA</a></li>
        <li class="nav" ><a href="#" >MUNDO</a></li>
        <li class="nav" ><a href="#" >ECONOMIA</a></li>
        <li class="nav" ><a href="#" >DEPORTES</a></li>
        <li class="nav" ><a href="#" >ENTRETET</a></li>
        <li class="nav" ><a href="#" >SALUD</a></li>
        <li class="clear"></li>
      </ul>
  </div>
</header>  


<div class="imagenslider" >
  <img  src="imgs/creadores.png" />
</div>

<div  class="contenedor principal">
  <div class="articulos">

        <div class="artic">
             <img  src="imgs/12.png" />
             <h2>Titulo 1</h2>
             <p class="texto" >La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.
             </p>             
             <div class="boton"><a href="#">ver mas</a></div>
            
        </div>
        <div class="artic">
            <img  src="imgs/13.png" />
            <h2>Titulo 2</h2>
            <p class="texto">La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.  </p>
            <div class="boton"><a href="#">ver mas</a></div>
         </div> 
        
        <div class="artic">
            <img  src="imgs/14.png" />
            <h2>Titulo 3</h2>
            <p class="texto" >La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.  </p>
            <div class="boton"><a href="#">ver mas</a></div>
        </div>
        <div class="clear"></div>
  </div>  

  <div class="noticias" style="background-color: white;">     
     
      <div style="float:left; width: 20%">
      
        <?php
          while($lista = mysql_fetch_assoc($lasnoticias))
        {
     ?>
     
     
     
          <div onclick="realizaProceso('<?php echo $lista['id'] ?>');"><?php echo $lista['titulo'] ?></div> 
          
          
          
             <?php
        }
        mysql_free_result($lasnoticias);  
    ?> 
          
        
      </div> 
          <div style="float:left; width: 80%" id="resultado">aaaa</div>
      <div style="clear:both"></div>      
   </div>

  <div class="testimonios"> 
    <div class="titulo">TESTIMONIOS</div>
    <div class="testim-contenido">
       <div class="testim-margen">
        <img  src="imgs/Gaby.png" />
        <div class="testim-titulo">
            <h2>titulo</h2>
            <p>tecnico computacion</p>
        </div>
      </div>
    </div>
    <div class="testim-contenido">
       <div class="testim-margen">
        <img  src="imgs/Carlos.png" />
        <div class="testim-titulo">
            <h2>titulo</h2>
            <p>tecnico computacion</p>
        </div>
      </div> 
    </div>
    <div class="testim-contenido">
       <div class="testim-margen">
        <img  src="imgs/Max.png" />
        <div class="testim-titulo">
            <h2>titulo</h2>
            <p>tecnico computacion</p>
        </div>
      </div>
    </div>
    <div class="testim-contenido">
       <div class="testim-margen">
        <img  src="imgs/Omar.png" />
        <div class="testim-titulo">
            <h2>titulo</h2>
            <p>tecnico computacion</p>
        </div>
      </div> 
    </div>
     <div class="clear"></div>
  </div>
  <div>
    <button onclick="document.getElementById('myImage').src='imgs/12.png'">Tecnologia</button>
    <button onclick="document.getElementById('myImage').src='imgs/13.png'">Gestion</button>
    <button onclick="document.getElementById('myImage').src='imgs/14.png'">Diseño</button>
    <img id="myImage" src="imgs/logo.png" style="width:100px">
  </div>

  <div>
    <br>
  <p><strong>Tecnologia informacion:</strong> Formamos profesionales técnicos competentes en las áreas de las tecnologías que brinden soluciones integrales para el crecimiento de la productividad de las organizaciones y que busquen el desarrollo continuo de sus competencias personales y laborales»</p>

  <p id="demo" style="display:none"><br>La Escuela de Tecnología enfatiza la formación orientada a desarrollar en los alumnos actitudes que conduzcan a la innovación, creatividad y apertura al cambio. De ese modo, se puede concebir la formación de egresados de tecnología como un recurso generador de valor para las empresas.</p>

  <button type="button" onclick="document.getElementById('demo').style.display='block'">Mas Informacion</button>
  <button type="button" onclick="document.getElementById('demo').style.display='none'">Ocultar Informacion</button>
  </div>
 
 <div style="padding: 10px;background-color: antiquewhite;"><p>Formulario contacto :</p><br>

  <form action="phpindex.php" method="post">
 <p>Su nombre: <input type="text" name="nombre" /></p>
 <p>Telefono: <input type="text" name="telefono" /></p>
 <p>Email: <input type="text" name="email" /></p>
 <p>Mensaje :</p>
 <p><textarea rows="4" cols="50" name="mensaje">
</textarea></p>

 <p><input type="submit" /></p>
</form>

</div>
<br>



  <div class="footer">
    <div class="imagen"> <img  src="imgs/footer.png" /></div>
    <div class="direccion"> Direccion: San Isidro</div>
    <div class="clear"> </div>
  </div> 


</div>

<script type="text/javascript" src="jquery-1.10.2.js"></script>
<script type="text/javascript" src="jsutil.js"></script>

<script>

</script>


</body>
</html>
