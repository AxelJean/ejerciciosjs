
$.ajax({

    url: "direccion.php",
    type: "post",

    beforeSend: function () {
              $(".direccionX").html("");
              },
    success: function(data){//data es los datos que se reciben
       $('.direccionX').html(data);
    }

})



$('#seleccionar').change(function(){
    $.ajax({
        url: "seleccionar.php",
        data: { "valor": $("#seleccionar").val() },
        type: "post",
        beforeSend: function () {
                  $(".prodseleccionado").html("");
                  },
        success: function(datos){
           $('.prodseleccionado').html(datos);
        }
    });
});




function listadoJsonbd() {      
         if (window.XMLHttpRequest) {           
             xmlhttp = new XMLHttpRequest();
         } else {           
             xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
         }
         xmlhttp.onreadystatechange = function() {            
             var result="<table border='1'>";
             if (this.readyState == 4 && this.status == 200) { 
                var misdatos = JSON.parse(this.responseText);  
               // [{"id":"1","nombre":"titulo1","imagen":"noticia1.jpg"},{"id":"2","nombre":"titulo 2","imagen":"noticia2.jpg"}]       
                misdatos.forEach(function(element)
                {  
                    result+= "<tr>";
                    result+= "<td>"+element.id+ "";
                    result+= "<td><img src='./imgs/"+element.imagen+"'>";
                    result+= "<td>"+element.nombre + "</td>";
                    result+= "<td><button type='button'  onclick='eliminar("+element.id+")' >eliminar</button></td>";
                    result+= "</tr>"	
                });
                result+="</table>"
                            
                 document.getElementById("listado").innerHTML = result;
             }
         };
         xmlhttp.open("GET","listadoJson.php",true);//http://localhost/paginaphp/listadoJson.php
         xmlhttp.send();    
}

//  html servicios :

const servicios= ()=> {      
    if (window.XMLHttpRequest) {           
        xmlhttp = new XMLHttpRequest();
    } else {           
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {            
        
        if (this.readyState == 4 && this.status == 200) { 
           var pagina = this.responseText;             
            document.getElementById("servicios").innerHTML = pagina;
        }
    };
    xmlhttp.open("GET","servicios.php",true);//http://localhost/paginaphp/listadoJson.php
    xmlhttp.send();    
}

listadoJsonbd();
servicios();

// ver productos
document.getElementById("losproductos").addEventListener("click", ()=> {
	 $.ajax({
             //data:  parametros,
             url:   'productoslist.php',
             type:  'get',
             beforeSend: function () {
              $(".productos").html("Procesando, espere por favor...");
             },
             success:  function (response) {
                    $(".productos").html(response); 
            }
     });
});


function registrar(){
   document.getElementById('formulario').style.display = "block";
   document.getElementById('lista').style.display = "none";
}


function ejecutar(){
    var titulo = document.getElementById('titulo').value;
    var texto = document.getElementById('texto').value;
    var fecha = document.getElementById('fecha').value;
    var imagen = document.getElementById('imagen').value;
 
    
    var ok = false;    
    if(titulo == ''){
    ok = false;
    }else{
         ok = true; 
    }
    
    if (ok==false){
        alert('falta');
        }
    else{
        realizaProceso(titulo, texto, fecha, imagen)
        }
}

function realizaProceso(valorCaja1, valorCaja2, valorCaja3, valorCaja4){
            var parametros = {
                    "valorCaja1" : valorCaja1,
                    "valorCaja2" : valorCaja2,
                    "valorCaja3" : valorCaja3,
                    "valorCaja4" : valorCaja4
            };
            $.ajax({
                    data:  parametros,
                    url:   'ejemplo_ajax_proceso.php',//1. Guardar
                    type:  'post',
                    beforeSend: function () {
                            $(".mensaje").html("Procesando, espere por favor...");
                    },
                    success:  function (response) {
                            $(".mensaje").html(response);//viene de la pag -> ejemplo_ajax_proceso.php //2. MOSTRAR
                   
                    // limpia input :
                    document.getElementById('titulo').value='';
                    document.getElementById('texto').value='';
                    document.getElementById('fecha').value='';
                    document.getElementById('imagen').value='';
                    $(".mensaje").html('');
                    // recarga listado para mostrarlo:
                     listadoJsonbd();//3 REFRESCAR EL LISTADO
                     }
            });

            //4. MOSTRAR EL LISTADO
            document.getElementById('lista').style.display = "block";
            document.getElementById('formulario').style.display = "none";
    }
    
    function eliminar(valor){
                var parametros = {
                        "id" : valor
                };
       
                $.ajax({
                        data:  parametros,
                        url:   'eliminarnot.php',
                        type:  'get',
                        beforeSend: function () {
                                $(".mensaje").html("Procesando, espere por favor...");
                        },
                        success:  function (response) {
                                $(".mensaje").html(response);
                            // recarga listado para mostrarlo:
                             listadoJsonbd();
                        }

                });
        }