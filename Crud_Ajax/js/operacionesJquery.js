$(function () {

	var app = {
		packages: [
			{ name: '1 a 30', price: 'S/ 169', onePay: 'S/ 180', total: 'S/ 349' },
			{ name: '31 a 60', price: 'S/ 244', onePay: 'S/ 360', total: 'S/ 604' },
			{ name: '61 a 100', price: 'S/ 366', onePay: 'S/ 600', total: 'S/ 966' },
			{ name: '101 a 200', price: 'S/ 898', onePay: 'S/ 2400', total: 'S/ 3298' },
		],
		isOpen: false,
		init: function ()
		{
			this.activatePlugins();
			this.bindEvents();
		},
        listado:function(){ // funcion de ayuda 
            $.ajax({
                type:'POST',
                url:'http://localhost/paginaphp/listadoJson.php',//listadoJson.php
                headers: {
                    // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success:function(data){
                    var datos = JSON.parse(data);
                    		
                        var result="<table >";
                        console.log('',datos)
                        datos.forEach(function(element)
                        { 							
                            result+= "<tr>";
                            result+= "<td>"+element.id+ "";
                            result+= "<td><img src='./imgs/"+element.imagen+"'>";
							result+= "<td>"+element.nombre+ "</td>";
							result+= "<td> <a href='#' class='delete' data-id="+ element.id +" >eliminar</a></td>";
                            result+= "</tr>"	
                                						
                        });
                        result+="</table>";
                        
                        $('#listado').html(result);
                   
        					  
                }	
            });	
        },
		activatePlugins: function ()
		{
			this.listado();
		},
		bindEvents: function ()
			{   
				$('.registrar').on('click', function(){
					
					$('#lista').hide();
					$('#formulario').show();
				});

				$('.enviar').on('click', function(){
				var dataform = $('#form').serialize();
				console.log(dataform);
					$.ajax({
						type:'POST',
						url:'procesarRegistroJ.php',
						data:dataform,
						headers: {
							//'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						success:function(data){
							app.listado();
							$('#lista').show();
							$('#formulario').hide();

							
						//	$('.libro-enviado').show().html(data.success);
						}
					});

				});	

				
				$(document).on('click', '.delete', function(e) {
					//e.preventDefault();					
					var item = $(this).data('id');
					console.log(item);
					var dataString = 'id='+item;
					
					$.ajax({
						type: "get",
						url: "eliminarnot.php",
						data: dataString,
						success: function(response) {			
							$(".mensaje").html(response);
							// recarga listado para mostrarlo:
							app.listado();
						}
					});
				});  

				$('#seleccionar').change(function(){
					$.ajax({
						url: "seleccionar.php",
						data: { "valor": $("#seleccionar").val() },
						type: "post",
						beforeSend: function () {
							      $(".prodseleccionado").html("");
							      },
						success: function(data){
						   $('.prodseleccionado').append(data);
						}
					});
				});

        }
	};
	app.init();
	
	});
