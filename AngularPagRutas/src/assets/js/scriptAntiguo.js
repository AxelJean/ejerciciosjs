$(function () {

	var app = {
		bannerTitles: [
			{title: '1. INVENTARIAMOS'},
			{title: '2. RECOGEMOS'},
			{title: '3. ALMACENAMOS'},
			{title: '4. ATENDEMOS'},
		],
		packages: [
			{ name: '1 a 30', price: 'S/ 169', onePay: 'S/ 180', total: 'S/ 349' },
			{ name: '31 a 60', price: 'S/ 244', onePay: 'S/ 360', total: 'S/ 604' },
			{ name: '61 a 100', price: 'S/ 366', onePay: 'S/ 600', total: 'S/ 966' },
			{ name: '101 a 200', price: 'S/ 898', onePay: 'S/ 2400', total: 'S/ 3298' },
		],
		isOpen: false,
		boton_seleccionarmov1: 0,
		containerHeight: 0,
		paso: 0,
		ruta: 0,
		cfiles: 0,

		init: function ()
		{
			this.activatePlugins();
			this.bindEvents();
		},

		activatePlugins: function ()
		{

			$('ul li:has(ul)').hover(function(e) {
				$(this).find('ul').css({display: "block"});
			},
			function(e) {
				$(this).find('ul').css({display: "none"});
			});

			/* tab*/
			


			
			var im = this;			
			// multiple file uploads
			$( '.inputfile' ).each( function()
			{
				var $input	 = $( this ),
					$label	 = $input.next( 'label' ),
					labelVal = $label.html();

				$input.on( 'change', function( e )
				{
                    var fileName = '';
                    app.cfiles=0;

					if( this.files && this.files.length > 1){
						fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
						app.cfiles = app.cfiles + this.files.length;
					}
						else if( e.target.value ) {
						app.cfiles = app.cfiles +this.files.length;
						fileName = e.target.value.split( '\\' ).pop();
						}
					if( fileName )
						$label.find( 'span' ).html( fileName );
					else
						$label.html( labelVal );
						console.log( this.files);// , fileName
						$('#files-contrato2').val(app.cfiles);
						$('.cantidad').text('Cantidad: '+ app.cfiles);
				});

				// Firefox bug fix
				$input
				.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
				.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
			});

		},

		doMovil: function (action, reference)
		{
			reference.removeClass('current-icon');
			reference.addClass('hidden-icon');
			reference.siblings('svg').toggleClass('current-icon hidden-icon');

			if (action == 'open') {
				$('header').addClass('opened');
				$('.nav-movil').slideDown();
			} else {
				$('header').removeClass('opened');
				$('.nav-movil').slideUp();
			}
		},
		openSelection: function (element)
		{
			const currentColumn = element.parent().attr('class');
			const columns = ['columna2','columna3','columna4','columna5'];

			for ( let column of columns){
				$('.' + column).removeClass("active");
				if ( column === currentColumn) {
					$('.' + column).addClass("active");
					$('.seguir-pasos, .contrato').addClass('active').siblings().removeClass('active');
					$('.tr-tipo-contrato, .tr-formulario-contrato').show().find('.seguir-pasos').addClass('active');
					$('.tr-formulario, .tr-formulario-servicio, .tr-formulario-contrato2, .tr-agradecer').hide();
					$('.contrato-jurid').hide()
					$('#natural-contrato').prop('checked',true);
					$('#juridica-contrato').prop('checked',false);
				}
			}

			if (element.hasClass('active') ){
				app.isOpen = false;
				element.removeClass('active');
				$('.btn-service').removeClass('active');
				$('.tr-formulario, .tr-formulario-servicio, .tr-tipo-contrato, .tr-formulario-contrato, .tr-formulario-contrato2, .tr-agradecer-pedido, .continua-paso3, .tr-agradecer').hide();
			} else {
				element.addClass('active').parent().siblings().find('.boton_seleccionar').removeClass('active');
				app.isOpen = true;
				$('.tr-formulario, .tr-formulario-servicio2').show();
			}

			var cajaindex =  element.data('select') - 2;

			$('.costocaja').text(app.packages[cajaindex].price);
			$('.pagocaja').text(app.packages[cajaindex].onePay);
            $('.totalcaja').text(app.packages[cajaindex].total);
            $('#paquete, #paquete-contrato2').val(app.packages[cajaindex].name);
			$('.tr-agradecer-pedido, .continua-paso3').hide();
			$('.titulo-form2').show();
			$('.cantidad').text('.');
			$('.steps').eq(0).addClass('active');

			app[element] = app[element] ? 0 : 1;
		},
		openService: function (element)
		{

			element.addClass('active').siblings().removeClass('active');

			if (element.hasClass('servicio')){
				$('.tr-formulario-servicio').show();
				$('.tr-formulario-contrato, .tr-tipo-contrato, .tr-formulario-contrato2').hide();
			} else {
				$('.tr-tipo-contrato, .tr-formulario-contrato').show();
				$('.tr-formulario-servicio, .tr-formulario-contrato2').hide();
				$('.seguir-pasos').addClass('active');
			}

			$('.tr-agradecer').hide();
			$('.tr-agradecer-pedido').hide();
			$('.datos-completo').removeClass('active');

		},
		openContract: function (element)
		{
			element.addClass('active').siblings().removeClass('active');

			if (element.hasClass('seguir-pasos')){
				$('.tr-formulario-contrato').show();
				$('.tr-formulario-contrato2').hide();
			} else {
				$('.tr-formulario-contrato').hide();
				$('.tr-formulario-contrato2').show();
			}
			$('.tr-agradecer-pedido').hide();
			$('.titulo-form2').show();

		}
		,
		closeService: function (element)
		{
			$('.tr-formulario-servicio').hide();
			$('.columna2 .servicio').removeClass('active');
		}
		,
		closeServiceEnvio: function (element)
		{
			$('.tr-formulario').hide();
			$('.tr-agradecer').hide();
			$('.tr-image .columna2, .tr-image .columna3, .tr-image .columna4, .tr-image .columna5').removeClass('active');
			$('.boton_seleccionar').removeClass('active');
			$('td.columna2,td.columna3,td.columna4,td.columna5').removeClass('active');
			$('.columna2 .servicio').removeClass('active');
		}
		,
		closePedidoContrato: function (element)
		{
			$('.tr-formulario, .tr-tipo-contrato, .tr-agradecer-pedido, .tr-agradecer').hide();
			$('.tr-image .columna2, .tr-image .columna3, .tr-image .columna4, .tr-image .columna5').removeClass('active');
			$('.boton_seleccionar').removeClass('active');
			$('td.columna2,td.columna3,td.columna4,td.columna5').removeClass('active');
			$('.columna2 .servicio').removeClass('active');
			$('.contrato').removeClass('active');
			$('.seguir-pasos').removeClass('bot-trespasos');
			$('#nombre-contrato2, #apellido-contrato2, #telefono-contrato2, #dni-contrato2').attr('type','text');
			$('.tr-formulario-contrato2 .dato').show();
			$('.continua-paso3, .contrato-jurid').hide();
			$('#natural-contrato').prop('checked',true);
			$('#juridica-contrato').prop('checked',false);
			//tr-agradecer-pedido  tr-tipo-contrato  seguir-pasos
		}
		,
		openCity:function()
		{
			
		}
		,



		onScroll: function()
		{
			let scrollTop = $(document).scrollTop();
			const scrollBtn = $(".scroll-btn");
			if (scrollTop > 10) $('.general-nav, header').addClass('thiner');
            if (scrollTop === 0) $('.general-nav, header').removeClass('thiner');

			scrollTop > $('.carousel-container').outerHeight() / 2 ? scrollBtn.addClass("blue") : scrollBtn.removeClass("blue");
            scrollTop + 200 > $(window).height() * 2 ? scrollBtn.hide() : scrollBtn.show();
		}
		,
		bindEvents: function ()
		{


			$('.responsive-nav-icon,.responsive-nav-icon-close').on('click',app.eventHandlers.toggleNav);

			

			$('.tablinks').on('click', function(){
				var pais=$(this).data('ruta');
			
			$('.tabcontent').html(pais);
			/*var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
			  tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
			  tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(pais).style.display = "block";*/
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
			  tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			$(this).addClass("active");

				
			});

			$('.btn-service').on('click', function(){
				app.openService($(this));
			});


			$('.logo').on('click', function () {				
				$('.carro').show();				
			});

			$('[data-nav-movil]').find('.mobil-svgs').find('svg.im-icon').on('click', function (object, param) {
				var reference = $(this);
				if (typeof param !== 'undefined') {
					app.doMovil(param, reference);
				} else {
					if ($('.im-icon.delete.current-icon').length > 0) {
						app.doMovil('close', reference);
					} else {
						app.doMovil('open', reference);
					}
				}
			});

			$('[data-movil-closeandgo]').on('click', function () {
				if ($(window).width() < 1004) {
					$('[data-nav-movil]').find('.mobil-svgs').find('svg.im-icon').trigger('click', 'close');
				}
			});

			//show scrollButton $('.scroll-btn')
			if($(window).height() > $('.carousel-container').outerHeight() + 200) $(".scroll-btn").addClass('blue');

			$(window).scroll(function () {
				app.onScroll();
			});

			$('.boton_seleccionar').on('click', function () {
				app.openSelection($(this));
			});

			$('.btn-service').on('click', function(){
				app.openService($(this));
			});

			$('.cerrar').on('click', function(){
				app.closeService($(this));
			});

			$('.cerrar-servicio').on('click', function(){
				app.closeServiceEnvio($(this));
			});

			$('.seguir-pasos, .datos-completo').on('click', function(){
				app.openContract($(this));
			});

			$('.cerrar-pedido').on('click', function(){
				app.closePedidoContrato($(this));
			});

			$('#juridica-servicio').on('change', function(){ /*radio*/
				$('.servicio-jurid').show();
			});

			$('#juridica-contrato').on('change', function(){
				$('.contrato-jurid').show();
			});

			$('#natural-servicio').on('change', function(){
				$('.servicio-jurid').hide();
			});

			$('#natural-contrato').on('change', function(){
				$('.contrato-jurid').hide();
			});


			$('.image-descarga, .desc').on('click', function(){
				$('.contrato-jurid-subir, .continua-paso3, .paso-3').show();
				$('.contrato-paso1, .continua-paso2').hide();
			});

			$('.enviar-form3').on('click', function(){ /*continuar a form contrato ahora*/
				$('.tr-formulario-contrato, .tr-formulario-contrato2 .dato, .titulo-form2').hide();
				$('.tr-formulario-contrato2, .aviso-paso3').show();
				$('#contrato-ahora .paso3').css('opacity', 'inherit');

                $('#nombre-contrato2').val($('#nombre-contrato').val());
                $('#apellido-contrato2').val($('#nombre-contrato').val());
				$('#telefono-contrato2').val($('#telefono-contrato').val());
                $('#dni-contrato2').val($('#dni-contrato').val());
                $('#email-contrato2').val($('#email-contrato').val());
                $('#iddni').val($('#dni-contrato').val());
				$('#nombre-contrato2, #apellido-contrato2, #telefono-contrato2, #dni-contrato2, #email-contrato2').attr('type','hidden');

				$('.steps').removeClass('active').eq(5).addClass('active');
				$('.seguir-pasos').removeClass('active');
				$('.datos-completo').addClass('active');
            });

            /* $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }); */
			/*  validacion */
			var carac_numer = "^[a-z A-Z 0-9]";
			var caracteres = "^[a-z A-Z]";/*{5,200}$*/
			var numerico = "^[ 0-9+ ]{7,30}$";
			var numericodni = "^[0-9]{8,8}$";
			var emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$";

			function Input(idInput, pattern) {
				return $(idInput).val().match(pattern) ? true : false;
			}
			function checkBox_radio(checkb) {
				return $(checkb).is(":checked") ? true : false;
			}

			$('.enviar-contratopc').on('click', function(){
                var validar = true;
                var ruta = $(this).data('ruta');
				$('.requerido').text('');
				if (!Input("#nombre-contrato", caracteres)) {
					$('#nombre-contrato').parent().find('.requerido').html('Ingresar nombre');
					validar = false;
                }
                if (!Input("#apellido-contrato", caracteres)) {
					$('#apellido-contrato').parent().find('.requerido').html('Ingresar apellidos');
					validar = false;
			    }
                if (!Input("#telefono-contrato", numerico)) {
                    $('#telefono-contrato').parent().find('.requerido').html('Ingresar teléfono válido');
                    validar = false;
                }
                if (!Input("#dni-contrato", numericodni)) {
                    $('#dni-contrato').parent().find('.requerido').html('Ingresar dni válido');
                    validar = false; ;
                }
                if (!Input("#direccion-contrato", carac_numer)) {
                    $('#direccion-contrato').parent().find('.requerido').html('Ingresar dirección válida');
                    validar = false;
                }
                if (!Input("#email-contrato", emailPattern)) {
                    $('#email-contrato').parent().find('.requerido').html('Ingresar email válido');
                    validar = false;
                }

                if (!Input("#country-contrato", caracteres)) {
					$('#country-contrato').parent().find('.requerido').html('Elegir país');
					validar = false;
                 }
                 if (!Input("#zipPostal-contrato", carac_numer)) {
					$('#zipPostal-contrato').parent().find('.requerido').html('Ingrese código postal');
					validar = false;
                 }

                if (!checkBox_radio("#cboxf1")) {
                    $('#cboxf1').parent().find('.requerido').html('Debe aceptar los términos y condiciones');
                    validar = false;
                }
                if (!checkBox_radio("#cboxdp")) {
                    $('#cboxdp').parent().find('.requerido').html('Debe aceptar los términos de uso de Datos Personales y Prácticas Anti-Soborno');
                    validar = false;
                }

			   if (checkBox_radio("#juridica-contrato")){
				   if (!Input("#razon-contrato", caracteres)) {
					   $('#razon-contrato').parent().find('.requerido').html('Ingresar Razón');
					   validar = false;
				   }
				   if (!Input("#ruc-contrato", numerico)) {
					   $('#ruc-contrato').parent().find('.requerido').html('Ingresar Ruc');
					   validar = false;
				   }
				   if (!Input("#cargo-contrato", caracteres)) {
					   $('#cargo-contrato').parent().find('.requerido').html('Ingresar Cargo');
					   validar = false;
				   }
			   }
			   if(validar == true){
                $('.upload').show().html('Un momento, por favor... <img src="img/uploading.gif" class="upload-img">');
                $('.enviar-contratopc').hide();
                $('.loading').show().html('Enviar');
                var dataform = $('#form-c1').serialize();
                $.ajax({
                   type:'POST',
                   url: ruta,
                   data:dataform,
                   headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                   success:function(data){
                       if (data) {
                        var emailConsentdato;
                            if ($('#EmailConsent-contrato:checked').val()){
                                emailConsentdato = "Yes";
                            }
                            else{
                                emailConsentdato = "No";
                            }
                            var companydato, titledato,citydato,areadato;
                            if ($('#form-c1 #razon-contrato').is(':visible')) {
                                companydato = $('#form-c1 #razon-contrato').val();
                            }
                            else{
                                companydato = "0";
                            }
                            if ($('#form-c1 #cargo-contrato').is(':visible')) {
                                titledato = $('#form-c1 #cargo-contrato').val();
                            } else {
                                titledato = "0";
                            }

                            if ($('#form-c1').find('#city-contrato').val() == "") {
                                citydato = "0";
                            } else {
                                citydato = $('#form-c1').find('#city-contrato').val();
                            }
                            if ($('#form-c1').find('#areaOfInterest1-contrato').val() == "") {
                                areadato = "0";
                            } else {
                                areadato = $('#form-c1').find('#areaOfInterest1-contrato').val();
                            }

                            var apiData = new FormData();
                            apiData.append('elqFormName', $('#form-c1').find('#elqFormName').val());
                            apiData.append('elqSiteID', $('#form-c1').find('#elqSiteID').val());
                            //apiData.append('elqCampaignId', $('#form-c1').find('#elqCampaignId').val());
                            apiData.append('firstName', $('#form-c1').find('#nombre-contrato').val());
                            apiData.append('lastName', $('#form-c1').find('#apellido-contrato').val());
                            apiData.append('busPhone', $('#form-c1').find('#telefono-contrato').val());
                            apiData.append('emailAddress', $('#form-c1').find('#email-contrato').val());
                            apiData.append('country', $('#form-c1').find('#country-contrato').val());

                            apiData.append('city', citydato);//vacio
                            apiData.append('zipPostal', $('#form-c1').find('#zipPostal-contrato').val());

                            apiData.append('EmailConsent', emailConsentdato);
                            apiData.append('areaOfInterest1', areadato);//vacio
                            apiData.append('company', companydato);
                            apiData.append('title', titledato);

                            $.ajax({
                                data: apiData,
                                url: "envioform",
                                method: "POST",
                                processData: false,
                                contentType: false,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success:function(data){
                                    if(data){
                                    }
                                }
                            });

                        $('.tr-formulario-contrato').hide();
                        $('input[type="text"]').val('');
                        $('.tr-agradecer-pedido, .tr-formulario-contrato2 .dato, .contrato-jurid-descarga1, .contrato-paso1').show();
                        $('.steps').removeClass('active').eq(1).addClass('active');
                        $('#cboxf1').prop('checked',false);
                        $('.pedido').html(data.success);
                        $('#EmailConsent-contrato, #cboxf1, #cboxdp').prop('checked', false);
                        $('.upload').hide();
                        $('.enviar-contratopc').show();
                        $('.loading').hide();
                      }
                   }
                });

			   }
	        });

			$('.enviar-contrato2').on('click', function(){
                var ruta = $(this).data('ruta');
				var file_c2 = $("#files-contrato2").val();
				var validar = true;
				$('.requerido').text('');
				$('.file-requerido-contrato2').hide();

				if (!Input("#nombre-contrato2", caracteres)) {
					$('#nombre-contrato2').parent().find('.requerido').html('Ingresar nombreo');
					validar = false;
                }
                if (!Input("#apellido-contrato2", caracteres)) {
					$('#apellido-contrato2').parent().find('.requerido').html('Ingresar apellidos');
					validar = false;
				}
				if (!Input("#telefono-contrato2", numerico)) {
					$('#telefono-contrato2').parent().find('.requerido').html('Ingresar teléfono válido');
					validar = false;
				}
				if (!Input("#dni-contrato2", numericodni)) {
					$('#dni-contrato2').parent().find('.requerido').html('Ingresar dni o ruc válido');
					validar = false;
                }
                if (!Input("#direccion-contrato2", carac_numer)) {
					$('#direccion-contrato2').parent().find('.requerido').html('Ingrese su dirección');
					validar = false;
                }
                if (!Input("#email-contrato2", emailPattern)) {
                    $('#email-contrato2').parent().find('.requerido').html('Ingresar email válido');
                    validar = false;
                }
                if (!Input("#country-contrato2", caracteres)) {
                    $('#country-contrato2').parent().find('.requerido').html('Elegir país');
                    validar = false;
                }
                if (!Input("#zipPostal-contrato2", carac_numer)) {
                    $('#zipPostal-contrato2').parent().find('.requerido').html('Ingresar código postal');
                    validar = false;
                }
				if (file_c2 != 1 ) {
					$('.file-requerido-contrato2').show();
					$('.file-requerido-contrato2').html('Subir Vaucher.');
					validar = false;
				}
				if (!checkBox_radio("#cboxf2")){
					$('#cboxf2').parent().find('.requerido').html('* Debe aceptar los términos y condiciones.');
					validar = false;
                }
                if (!checkBox_radio("#cboxdp2")){
					$('#cboxdp2').parent().find('.requerido').html('* Debe aceptar los términos de uso de Datos Personales y Prácticas Anti-Soborno.');
					validar = false;
				}
					if(validar == true){
                        var archivos = new FormData($('#contrato-ahora')[0])
                        var ins = document.getElementById('file').files.length;
                        for (var x = 0; x < ins; x++) {
                            archivos.append("file[]", document.getElementById('file').files[x]);
                        }
                        $('.upload').show().html('Un momento, por favor... <img src="img/uploading.gif" class="upload-img">');
                        $('.enviar-contrato2').hide();
                        $('.loading').show().html('Enviar');
                        $.ajax({
                            type:'POST',
                            url:ruta,
                            data: archivos,
                            contentType:false,
                            cache: false,
                            processData:false,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success:function(data){
                            if(data){
                                    /* servidor externo */
                                var emailConsentdato;
                                if ($('#EmailConsent-contrato2:checked').val()){
                                    emailConsentdato = "Yes";
                                }
                                else{
                                    emailConsentdato = "No";
                                }
                                var companydato, titledato,citydato,areadato;
                                if ($('#contrato-ahora #razon-contrato2').is(':visible')) {
                                    companydato = $('#contrato-ahora #razon-contrato2').val();
                                }
                                else{
                                    companydato = "0";
                                }
                                if ($('#contrato-ahora #cargo-contrato2').is(':visible')) {
                                    titledato = $('#contrato-ahora #cargo-contrato2').val();
                                } else {
                                    titledato = "0";
                                }

                                if ($('#contrato-ahora').find('#city-contrato2').val() == "") {
                                    citydato = "0";
                                } else {
                                    citydato = $('#contrato-ahora').find('#city-contrato2').val();
                                }
                                if ($('#contrato-ahora').find('#areaOfInterest1-contrato2').val() == "") {
                                    areadato = "0";
                                } else {
                                    areadato = $('#contrato-ahora').find('#areaOfInterest1-contrato2').val();
                                }

                               var apiData = new FormData();
                                apiData.append('elqFormName', $('#contrato-ahora').find('#elqFormName').val());
                                apiData.append('elqSiteID', $('#contrato-ahora').find('#elqSiteID').val());
                                //apiData.append('elqCampaignId', $('#form-c1').find('#elqCampaignId').val());
                                apiData.append('firstName', $('#contrato-ahora').find('#nombre-contrato2').val());
                                apiData.append('lastName', $('#contrato-ahora').find('#apellido-contrato2').val());
                                apiData.append('busPhone', $('#contrato-ahora').find('#telefono-contrato2').val());
                                apiData.append('emailAddress', $('#contrato-ahora').find('#email-contrato2').val());
                                apiData.append('country', $('#contrato-ahora').find('#country-contrato2').val());

                                apiData.append('city',citydato );//vacio
                                apiData.append('zipPostal', $('#contrato-ahora').find('#zipPostal-contrato2').val());

                                apiData.append('EmailConsent', emailConsentdato);
                                apiData.append('areaOfInterest1', areadato);//vacio
                                apiData.append('company', companydato);
                                apiData.append('title', titledato);
                                $.ajax({
                                    data: apiData,
                                    url: "envioform",
                                    method: "POST",
                                    processData: false,
                                    contentType: false,
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    success:function(data){
                                        if(data){
                                        }
                                    }
                                });
                                    app.cfiles = 0;
                                    $('#nombre-contrato2, #apellido-contrato2, #telefono-contrato2, #dni-contrato2, #email-contrato2').attr('type','text');
                                    $('input[type="text"]').val('');
                                    $("#file").val('');
                                    $("#cboxf2, #EmailConsent-contrato2, #cboxdp2").prop('checked', false);
                                    $('.tr-formulario-contrato2, .aviso-paso3, .contrato-jurid-descarga, .contrato-jurid-subir, .continua-paso3 ').hide();
                                    $('.tr-agradecer-pedido, .tr-formulario-contrato2 .dato, .contrato-jurid-descarga1, .contrato-paso1').show();
                                    $("#cboxf1").removeAttr('cheked');
                                    $('#files-contrato2').val('0');
                                    $('#iddni').val('');
                                    $('.upload').hide();
                                    $('.enviar-contrato2').show();
                                    $('.loading').hide();
                                    $('.pedido').html(data.success);
                               }
                            }
                         });
					 }
            });

			$('.enviar-serviciopc').on('click', function(){
                var ruta = $(this).data('ruta');
                var validar = true;
				$('.requerido').text('');
				if (!Input("#name", caracteres)) {
					$('#name').parent().find('.requerido').html('Ingresar nombre');
					validar = false;
				}
				if (!Input("#lastname", caracteres)) {
					$('#lastname').parent().find('.requerido').html('Ingresar apellido');
					validar = false;
			    }
				if (!Input("#phone", numerico)) {
					$('#phone').parent().find('.requerido').html('Ingresar teléfono válido');
					validar = false;
				}
				if (!Input("#dni", numericodni)) {
					$('#dni').parent().find('.requerido').html('Ingresar dni válido');
					validar = false;
				}
				if (!Input("#address", carac_numer)) {
					$('#address').parent().find('.requerido').html('Ingresar dirección válida');
					validar = false;
                 }
                 if (!Input("#email", emailPattern)) {
					$('#email').parent().find('.requerido').html('Ingresar email válido');
					validar = false;
				 }
                 if (!Input("#country", caracteres)) {
					$('#country').parent().find('.requerido').html('Elegir país');
					validar = false;
                 }
                 if (!Input("#zipPostal", carac_numer)) {
					$('#zipPostal').parent().find('.requerido').html('Ingrese código postal');
					validar = false;
                 }

                 if (!checkBox_radio("#cboxf")) {
                    $('#cboxf').parent().find('.requerido').html('Debe aceptar los términos y condiciones');
                    validar = false;
                }
				 if (checkBox_radio("#juridica-servicio")){
					if (!Input("#reason", caracteres)) {
						$('#reason').parent().find('.requerido').html('Ingresar Razón social');
						validar = false;
					}
					if (!Input("#ruc", numerico)) {
						$('#ruc').parent().find('.requerido').html('Ingresar Ruc');
						validar = false;
					}
					if (!Input("#position", caracteres)) {
						$('#position').parent().find('.requerido').html('Ingresar Cargo');
						validar = false;
					}
				}
				if (validar == true){
                    $('.upload').show().html('Un momento, por favor... <img src="img/uploading.gif" class="upload-img">');
                    $('.enviar-serviciopc').hide();
                    $('.loading').show().html('Enviar');
                    var dataform = $('#form-servicios').serialize();
                    $.ajax({
                       type:'POST',
                       url:ruta,
                       data:dataform,
                       headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                       success:function(data){
                          if (data) {
                            var emailConsentdato;
                            if ($('#EmailConsent:checked').val()){
                                emailConsentdato = "Yes";
                            }
                            else{
                                emailConsentdato = "No";
                            }
                            var companydato, titledato, citydato, areadato;
                            if ($('#form-servicios #reason').is(':visible')) {
                                companydato = $('#form-servicios #reason').val();
                            }
                            else{
                                companydato = "0";
                            }
                            if ($('#form-servicios #position').is(':visible')) {
                                titledato = $('#form-servicios #position').val();
                            } else {
                                titledato = "0";
                            }
                            if ($('#form-servicios').find('#city').val() == "") {
                                citydato = "0";
                            } else {
                                citydato = $('#form-servicios').find('#city').val();
                            }
                            if ($('#form-servicios').find('#areaOfInterest1').val() == "") {
                                areadato = "0";
                            } else {
                                areadato = $('#form-servicios').find('#areaOfInterest1').val();
                            }
                           var apiData = new FormData();
                            apiData.append('elqFormName', $('#form-servicios').find('#elqFormName').val());
                            apiData.append('elqSiteID', $('#form-servicios').find('#elqSiteID').val());
                            //apiData.append('elqCampaignId', $('#form-servicios').find('#elqCampaignId').val());
                            apiData.append('firstName', $('#form-servicios').find('#name').val());
                            apiData.append('lastName', $('#form-servicios').find('#lastname').val());
                            apiData.append('busPhone', $('#form-servicios').find('#phone').val());
                            apiData.append('emailAddress', $('#form-servicios').find('#email').val());
                            apiData.append('country', $('#form-servicios').find('#country').val());

                            apiData.append('city', citydato);//vacio
                            apiData.append('zipPostal', $('#form-servicios').find('#zipPostal').val());

                            apiData.append('EmailConsent', emailConsentdato);
                            apiData.append('areaOfInterest1', areadato);//vacio
                            apiData.append('company', companydato);
                            apiData.append('title', titledato);
                            $.ajax({
                                data: apiData,
                                url: "envioform",
                                method: "POST",
                                processData: false,
                                contentType: false,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success:function(data){
                                    if(data){
                                    }
                                }
                            });
                            $('#natural-servicio').prop('checked',true);
                            $('#juridica-servicio').prop('checked',false);
                            $('.servicio-jurid').hide();
                            $('input[type="text"]').val('');
                            $('.tr-formulario-servicio').hide();
                            $('.tr-agradecer').show();
                            $('#EmailConsent, #cboxf').prop('checked',false);
                            $('.upload').hide();
                            $('.enviar-serviciopc').show();
                            $('.loading').hide();
                            }
                       },
                        error: function() {
                            $('.form-information').text('Ocurrido inconveniente, intentarlo luego');
                        }
                    });
				}
            });

            $('.enviar-libro').on('click', function(){
                var ruta = $(this).data('ruta');
				var validar = true;
                $('.requerido').text('');

				if (!Input("#nombre-libro", caracteres)) {
					$('#nombre-libro').parent().find('.requerido').html('Ingresar nombre y apellidos');
					validar = false;
                }
                if (!Input("#domicilio-libro", carac_numer)) {
					$('#domicilio-libro').parent().find('.requerido').html('Ingresar domicilio');
					validar = false;
                }
                if (!Input("#dni-libro", numericodni)) {
					$('#dni-libro').parent().find('.requerido').html('Ingresar dni válido');
					validar = false;
                }
                if (!Input("#telefono-libro", numerico)) {
					$('#telefono-libro').parent().find('.requerido').html('Ingresar teléfono válido');
					validar = false;
                }
                if (!Input("#email-libro", emailPattern)) {
					$('#email-libro').parent().find('.requerido').html('Ingresar email válido');
					validar = false;
			    }
                if (!checkBox_radio("#producto") && !checkBox_radio("#servicio")){
                   $('#producto').parent().find('.requerido').html('Elegir una opción');
                    validar = false;
                }
                if (!Input("#descripcion-libro", carac_numer)) {
					$('#descripcion-libro').parent().find('.requerido').html('Ingresar la descripción.');
					validar = false;
                }
                if (!checkBox_radio("#reclamo") && !checkBox_radio("#queja")){
                    $('#reclamo').parent().find('.requerido').html('Elegir una opción.');
                     validar = false;
                 }
                if (!Input("#detalle-libro", carac_numer)) {
					$('#detalle-libro').parent().find('.requerido').html('Ingresar el detalle.');
					validar = false;
                }
                if (!Input("#pedido-libro", carac_numer)) {
					$('#pedido-libro').parent().find('.requerido').html('Ingresar el pedido.');
					validar = false;
                }
                if (!Input("#nombre-libro", caracteres)) {
					validar = false;
                }

                if (!grecaptcha.getResponse()){
                    $('.captcha').html('No coinciden, intentar nuevamente');
                    validar = false;
                }

                if (validar == true){
                    var dataform = $('#form-libro').serialize();
                   $.ajax({
                       type:'POST',
                       url:ruta,
                       data:dataform,
                       headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                       success:function(data){
                            $('.container-form').hide();
                            $('.libro').show();
                            $('.container-book').addClass('book-backg');
					        $('.libro-enviado').show().html(data.success);
                       }
                    });
				}
			});











			$('.agregar-libro').on('click', function(){

                var ruta = $(this).data('ruta');
				var validar = true;
                $('.requerido').text('');
				
               if (!Input("#cantidad", carac_numer)) {
					$('#formulario').parent().find('.requerido').html('Ingresar cantidad numerico');
					validar = false;
                } 

                if (validar == true){
                    var dataform = $('#form-libro').serialize();
					$.ajax({
						type:'POST',
						url:ruta,
						data:dataform,
						headers: {
							// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
						success:function(data){
								$('.content-formulario').hide();
								$('.mensaje-agregar').show();
								$('.listado').show();
							/* $('.container-book').addClass('book-backg');
								$('.libro-enviado').show().html(data.success);*/
						}
					});

					/*listado*/
					$.ajax({
						type:'POST',
						url:'procesarListado.php',
						/*data:dataform,*/
						headers: {
							// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
						success:function(data){
							   $('.listado').html(data);
								$('.content-formulario').hide();
								$('.mensaje-agregar').show();
							/* $('.container-book').addClass('book-backg');
								$('.libro-enviado').show().html(data.success);*/
						}
					});

				}
			});













			$('input[type="text"]').focus(function() {
				const elem = $(this);
				elem.siblings('.placeholder').addClass('active').html(elem.attr('placeholder'));
				elem.attr('placeholder', '')
			}).blur(function(){
				const elem = $(this);
				const placeholder = $(this).siblings('.placeholder').text();
				elem.siblings('.placeholder').removeClass('active').html('');
				elem.attr('placeholder', placeholder);
			});
			/**/
			$('#opencuenta, #opencuenta2').on('click', function () {
				  $('body').addClass('popup');
				  $('.account').show();
			});
			$('.closepop').on('click', function () {
				$('body').removeClass('popup');
				 $('.account').hide();
            });

			$('a[href*="#"]')
			.not('[href="#"]')
			.not('[href="#0"]')
			.click(function(event) {
				if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
					&& location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
					if (target.length) {
						event.preventDefault();
						$('html, body').animate({
							scrollTop: target.offset().top
						}, 1000);
					}
				}
			});
		},
		eventHandlers: {
			toggle: function(event) {
				var $elem = $(event.currentTarget);
				var config = $elem.data('jsToggle');
				$elem.toggleClass('is-active');
				$(config.targetSelector).toggleClass(config.className);
				return false;
			},
	
			toggleNav: function() {
				$('.responsive-nav-menu').toggleClass('responsive-nav-menu-active');
			},
	
			scrollTop: function() {
				$('html, body').animate({ scrollTop: 0 });
			},
	
			scrollUp: function(e) {
				var elem = $(e.currentTarget),
					elemSiblings = elem.siblings(".footer-list");
				elemSiblings.parent('.responsive-heading').siblings('.responsive-heading').find('.footer-list').siblings(".responsive-arrow").removeClass("responsive-arrow-rotate");
				elem.toggleClass("responsive-arrow-rotate");
				elemSiblings.parent('.responsive-heading').siblings('.responsive-heading').find('.footer-list').slideUp("200");
				elemSiblings.slideToggle( "200");
			},
	
			accordion: function(event) {
				var $elem = $(event.target);
				var $btns = $(event.delegateTarget).find('.accordion-btn');
				$btns.not($(this)).removeClass('is-open');
				$(this).toggleClass('is-open');
			},
	
			goBack: function(event) {
				history.back();
			},
	
			selectUrl: function(event) {
				var $elem = $(event.currentTarget);
				location.href = $elem.val();
			},
	
			closeAlert: function (event) {
				var $elem = $(event.currentTarget);
				$elem.closest('.alert').remove();
			},
	
			toggleSearch:function() {
				$('.nav-search,.nav-search-close,.search-form').toggleClass("active");
			},
	
			slickPrev: function() {
				$(".first-slider, .second-slider").slick('slickNext');
			},
	
			slickNext: function() {
				$(".first-slider, .second-slider").slick('slickPrev');
			},
	
			showSubOperations: function(e) {
				var $elem = $(e.currentTarget);
	
				if ($elem.hasClass('mapacoimolache')) {
					$elem.siblings('.mapa-imagen-alt').addClass('is-visible');
					$('.mapabrocal, .mapabrocal2').show();
				}
	
				if (typeof app.subOperationsTimeout !== 'undefined') {
					clearTimeout(app.subOperationsTimeout);
				}
			},
	
			hideSubOperations: function(e) {
				var $elem = $(e.currentTarget);
				app.subOperationsTimeout = setTimeout(function() {
					$elem.siblings('.mapa-imagen-alt').removeClass('is-visible');
					$('.mapabrocal, .mapabrocal2').hide();
				}, 2000);
			},
	
			checkScroll: function() {
				if ($(window).scrollTop() === 0) {
					$('.nav-scrolltop').removeClass('nav-scrollbottom');
				}
				else {
					$('.nav-scrolltop').addClass('nav-scrollbottom');
				}
			}
		}
	};
	app.init();
		$('#juridica-contrato').prop('checked',false);
		$('#juridica-servicio').prop('checked',false);
		$('#natural-contrato').prop('checked',true);
		$('#natural-servicio').prop('checked',true);
	});
