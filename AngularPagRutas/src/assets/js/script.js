$(function () {
	var app = {
		/*packages: [
			{ name: '1 a 30', price: 'S/ 169', onePay: 'S/ 180', total: 'S/ 349' },
			{ name: '31 a 60', price: 'S/ 244', onePay: 'S/ 360', total: 'S/ 604' },
			{ name: '61 a 100', price: 'S/ 366', onePay: 'S/ 600', total: 'S/ 966' },
			{ name: '101 a 200', price: 'S/ 898', onePay: 'S/ 2400', total: 'S/ 3298' },
		],
		isOpen: false,
		paso: 0,*/

		/* Orden se va Ejecutar */
		init: function ()
		{
			this.activatePlugins();
			this.bindEvents();
		},
		activatePlugins: function ()
		{						
			var pais=1;
			var parametros = {"Idx": pais,};
			$.ajax({
				data: parametros,
				type:'GET',
				url:'idproducto.php',
				/*data:dataform,*/
				headers: {
					// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
				success:function(data){//luego de la consulata a la BD
					$('.tabcontent').html(data);
				}
			});
		},
		
		/* Aca se definen los Eventos*/
		bindEvents: function ()
		{
			$('.responsive-nav-icon,.responsive-nav-icon-close').on('click',app.eventHandlers.toggleNav);

			$('.logo').on('click', function () {				
				$('.carro').show();				
			});

			/*  validacion */
			var carac_numer = "^[a-z A-Z 0-9]";
			var caracteres = "^[a-z A-Z]";/*{5,200}$*/
			var numerico = "^[ 0-9+ ]{7,30}$";
			var numericodni = "^[0-9]{8,8}$";
			var emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$";

			function Input(idInput, pattern) {
				return $(idInput).val().match(pattern) ? true : false;
			}
			function checkBox_radio(checkb) {
				return $(checkb).is(":checked") ? true : false;
			}

			$('.agregar-libro').on('click', function(){

                var ruta = $(this).data('ruta');
				var validar = true;
                $('.requerido').text('');
				
               if (!Input("#cantidad", carac_numer)) {
					$('#formulario').parent().find('.requerido').html('Ingresar cantidad numerico');
					validar = false;
                } 

                if (validar == true){
                    var dataform = $('#form-libro').serialize();
					$.ajax({
						type:'POST',
						url:ruta,
						data:dataform,
						headers: {
							// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
						success:function(data){
								$('.content-formulario').hide();
								$('.mensaje-agregar').show();
								$('.listado').show();
							/* $('.container-book').addClass('book-backg');
								$('.libro-enviado').show().html(data.success);*/
						}
					});

					/*listado*/
					$.ajax({
						type:'GET',
						url:'procesarListado.php',
						/*data:dataform,*/
						headers: {
							// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
						success:function(data){
							   $('.listado').html(data);
								$('.content-formulario').hide();
								$('.mensaje-agregar').show();
							/* $('.container-book').addClass('book-backg');
								$('.libro-enviado').show().html(data.success);*/
						}
					});

				}
			});
			/*Ver el listado productos */
			$('.vercarro').on('click', function(){				
				$.ajax({
					type:'POST',
					url:'CarroVacioOLleno.php',
					/*data:dataform,*/
					headers: {
						// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					success:function(data){
						var datos = JSON.parse(data);
						if(datos.respuesta =='NO'){
							$('.listado').html("carro vacio");
						}else{				
							var $total = 0;			
							var result="<table class='table'>";
							console.log(datos.lista)
							datos.lista.forEach(function(element)
							{ 							
								result+= "<tr>";
								result+= "<td style='width:30%'>";
								result+= "<img style='width:50%' src='./imagenes/"+element.Imagen+"'>";
								result+= "<td style='width:70%;color:#000000'>";
								result+= "<span>"+element.Nombre+ "</span> <br>"	
								result+= "<strong>Precio : </strong>S/. " + element.Precio + "<br>";
								result+= "<strong>Cantidad </strong>:  " + element.Cantidad + "<br>";
								result+= "<strong>Total :</strong> S/."+ element.Cantidad*element.Precio +"<br>";                           
								result+= "</td></tr>";
								result+= "<tr><td colspan=3><hr><td></tr>";
								$total=(element.Cantidad*element.Precio)+$total;						
							});
							result+="</table>"
							result+=" Total: S/. "+$total+"";
							result+="<br><br><a style='width: 200px; color: #21201f;text-align: center;' href='./carritodecompras.php'>Ver el carrito</a>";
							$('.listado').html(result);
						}
						$('.listado').toggle();						  
					}	
				});		
			})

			/*Aca defino el evento Tab Categorias */
			$('.tablinks').on('click', function(){
				var pais=$(this).data('ruta');
				var parametros = {"Idx": pais,};
				$.ajax({
					data: parametros,
					type:'GET',
					url:'idproducto.php',
					/*data:dataform,*/
					headers: {
						// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
					success:function(data){//luego de la consulata a la BD
						$('.tabcontent').html(data);
					}
				});
								
				tablinks = document.getElementsByClassName("tablinks");
				for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
				}
				$(this).addClass("active");				
			});


		},
		eventHandlers: {
			toggle: function(event) {
				var $elem = $(event.currentTarget);
				var config = $elem.data('jsToggle');
				$elem.toggleClass('is-active');
				$(config.targetSelector).toggleClass(config.className);
				return false;
			},
	
			toggleNav: function() {
				$('.responsive-nav-menu').toggleClass('responsive-nav-menu-active');
			}			
		}
	};
	app.init();	

	});
