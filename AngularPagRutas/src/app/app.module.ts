import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { PaglibroComponent } from './paglibro/paglibro.component';
import { PagscotiabankComponent } from './pagscotiabank/pagscotiabank.component';

@NgModule({
  declarations: [
    AppComponent,
    PaglibroComponent,
    PagscotiabankComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
