import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { PaglibroComponent } from './paglibro/paglibro.component';
import { PagscotiabankComponent } from './pagscotiabank/pagscotiabank.component';


const routes: Routes = [

  {
    path:'paglibro',
    component: PaglibroComponent
  },

  {
    path:'pagscotiabank',
    component: PagscotiabankComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
