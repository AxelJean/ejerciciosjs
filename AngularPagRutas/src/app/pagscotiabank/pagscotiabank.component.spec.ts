import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagscotiabankComponent } from './pagscotiabank.component';

describe('PagscotiabankComponent', () => {
  let component: PagscotiabankComponent;
  let fixture: ComponentFixture<PagscotiabankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagscotiabankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagscotiabankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
