import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaglibroComponent } from './paglibro.component';

describe('PaglibroComponent', () => {
  let component: PaglibroComponent;
  let fixture: ComponentFixture<PaglibroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaglibroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaglibroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
